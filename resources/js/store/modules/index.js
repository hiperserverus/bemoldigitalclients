import auth from './auth';
import snackbar from './snackbar';

export default {
  auth,
  snackbar,
};

export default {
  226: 'Error 226: Alguno de los datos proporcionado está siendo usado en otro registro.',
  400: 'Error 400: El usuario y/o contraseña es inválido.',
  401: 'Error 401: El usuario no existe.',
  404: 'Error 404: No encontrado.',
  408: 'Error 408: Tiempo de espera superado.',
  500: 'Error 500: Se ha encontrado algún problema con el servidor.',
};
